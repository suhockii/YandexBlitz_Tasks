package app.suhocki.circletransform;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class CircleTransform {

    public static class Point {
        final double x;
        final double y;

        Point(double x, double y) {
            this.x = x;
            this.y = y;
        }
    }

    public enum ActionType {
        ACTION_DOWN, ACTION_POINTER_DOWN, ACTION_MOVE, ACTION_POINTER_UP, ACTION_UP, ACTION_CANCEL
    }

    private static final String FILE_PATH = "C:\\Users\\Maksim.Suhotskiy\\AndroidStudioProjects\\JavaApp\\circletransform\\src\\main\\java\\app\\suhocki\\circletransform\\input.txt";
    private static final String SPACE = " ";
    private static final double UNDEFINED = Double.MIN_VALUE;
    private static final double FIRST_FINGER = 0;

    public static void main(String[] args) throws IOException {
        Point circleCenter = null;
        double circleRadius = UNDEFINED;
        Point rotationPoint = null;

        Point downPoint = null;
        Point pointerDownPoint = null;
        Point movePoint = null;
        Point pointerUp = null;

        for (String line : readInputFile()) {
            String[] elements = line.split(SPACE);

            if (circleCenter == null || circleRadius == UNDEFINED) {
                circleCenter = new Point(Double.parseDouble(elements[0]), Double.parseDouble(elements[1]));
                circleRadius = Double.parseDouble(elements[2]);
                continue;
            }

            if (rotationPoint == null) {
                rotationPoint = new Point(Double.parseDouble(elements[0]), Double.parseDouble(elements[1]));
                continue;
            }

            switch (ActionType.valueOf(elements[3])) {
                case ACTION_DOWN:
                    if (downPoint == null) {
                        downPoint = new Point(Double.parseDouble(elements[1]), Double.parseDouble(elements[2]));
                    }
                    break;

                case ACTION_POINTER_DOWN:
                    if (pointerDownPoint == null) {
                        pointerDownPoint = new Point(Double.parseDouble(elements[1]), Double.parseDouble(elements[2]));
                    }
                    break;

                case ACTION_MOVE:
                    if (Double.parseDouble(elements[0]) == FIRST_FINGER) {
                        movePoint = new Point(Double.parseDouble(elements[1]), Double.parseDouble(elements[2]));
                    }
                    break;

                case ACTION_POINTER_UP:
                    if (pointerUp == null) {
                        pointerUp = new Point(Double.parseDouble(elements[1]), Double.parseDouble(elements[2]));
                    }
                    break;
            }
        }

        double radius = calculateRadius(circleRadius, downPoint, pointerDownPoint, movePoint, pointerUp);
        Point center = calculateCenter(circleCenter, rotationPoint, downPoint, pointerDownPoint, movePoint, pointerUp);
        System.out.println(String.valueOf(Math.round(center.x)) + SPACE +
                Math.round(center.y) + SPACE +
                Math.round(radius));
    }

    private static Point calculateCenter(Point circleCenter, Point rotationPoint, Point p1, Point p2,
                                         Point p3, Point p4) {
        double a1 = p1.y - p2.y;
        double b1 = p2.x - p1.x;

        double a2 = p3.y - p4.y;
        double b2 = p4.x - p3.x;

        double phi = Math.acos((b1 * b2 + a1 * a2) /
                (Math.sqrt(b1 * b1 + a1 * a1) * Math.sqrt(b2 * b2 + a2 * a2)));

        Point relativeCoords = new Point(circleCenter.x - rotationPoint.x, circleCenter.y - rotationPoint.y);
        Point relativeCenter = new Point(
                relativeCoords.x * Math.cos(phi) - relativeCoords.y * Math.sin(phi),
                relativeCoords.x * Math.sin(phi) + relativeCoords.y * Math.cos(phi)
        );

        return new Point(relativeCenter.x + rotationPoint.x, relativeCenter.y + rotationPoint.y);
    }

    private static Double calculateRadius(Double oldRadius, Point downPoint, Point pointerDownPoint,
                                          Point movePoint, Point pointerUp) {
        double was = Math.sqrt(Math.pow(pointerDownPoint.x - downPoint.x, 2) + Math.pow(pointerDownPoint.y - downPoint.y, 2));
        double actual = Math.sqrt(Math.pow(pointerUp.x - movePoint.x, 2) + Math.pow(pointerUp.y - movePoint.y, 2));

        return oldRadius * actual / was;
    }

    private static String[] readInputFile() throws IOException {
        String input = readFile(FILE_PATH, StandardCharsets.UTF_8);
        return Arrays.stream(input.split("\\n"))
                .toArray(String[]::new);
    }

    private static String readFile(String path, Charset encoding)
            throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

}