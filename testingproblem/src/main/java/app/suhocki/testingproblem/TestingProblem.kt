package app.suhocki.testingproblem

fun main(args: Array<String>) {
    val taskCount = readLine()!!.toInt()
    val deviceCount = readLine()!!.toInt()
    val timePerTask = mutableListOf<Int>()
    repeat(taskCount) {
        timePerTask.add(readLine()!!.toInt())
    }
    timePerTask.sortDescending()

    val grouped = timePerTask.withIndex()
        .groupBy { it.index / deviceCount }
        .map { it.value.map { it.value } }

    var totalTime = 0
    grouped.forEachIndexed { x, subList ->
        subList.forEach {
            totalTime += (2 * x + 1) * it
        }
    }

    println(totalTime)
}
